<?php

namespace App\Controller;

use App\Entity\BlogPost;
use App\Form\ArchiveFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class BlogController extends AbstractController
{
    private $entityManager;
    private $userRepository;
    private $blogPostRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->blogPostRepository = $entityManager->getRepository('App:BlogPost');
        $this->userRepository = $entityManager->getRepository('App:User');
    }

    /**
     * 
     * @Route("blog/create", name="create_post", methods={"GET","POST"})
     */
    public function createAction(Request $request): Response
    {
        $title = "Create a post";
        $blogPost = new BlogPost();
        $author = $this->userRepository->findOneBy(['account' => $this->getUser()->getAccount()]);
        $blogPost->setAuthor($author);
        $form = $this->createForm(ArchiveFormType::class, $blogPost);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($blogPost);
            $entityManager->flush();

            return $this->redirectToRoute('index');
        }

        return $this->render('blog/new.html.twig', [
            'title' => $title,
            'blogPost' => $blogPost,
            'form' => $form->createView(),
            'author' => $author
        ]);
    }

    /**
     * @Route("", name="index")
     * @Route("blog/archives", name="archives", methods={"GET"})
     */
    public function archivesAction()
    {
        return $this->render('blog/archives.html.twig', [
            'blogPosts' => $this->blogPostRepository->findAll()
        ]);
    }

    /**
     * @Route("blog/{slug}", name="view_post", methods={"GET"})
     */
    public function viewAction(string $slug)
    {
        return $this->render('blog/post.html.twig', [
            'blogPost' => $this->blogPostRepository->findOneBy(['slug' => $slug])
        ]);
    }

    /**
     * 
     * @Route("blog/{slug}/edit", name="edit_post", methods={"GET","POST"})
     */
    public function editAction(Request $request, string $slug): Response
    {
        $blogPost = $this->blogPostRepository->findOneBy(['slug' => $slug]);
        $title = "Edit post";

        $author = $this->userRepository->findOneBy(['account' => $this->getUser()->getAccount()]);

        if (!$blogPost || $author !== $blogPost->getAuthor()) {
            $this->addFlash('error', 'You must be the author to edit this post');

            return $this->redirectToRoute('index');
        }

        $form = $this->createForm(ArchiveFormType::class, $blogPost);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('view_post', [
                'slug' => $blogPost->getSlug()
            ]);
        }

        return $this->render('blog/new.html.twig', [
            'title' => $title,
            'blogPost' => $blogPost,
            'form' => $form->createView(),
        ]);
    }

    /**
     * 
     * @Route("blog/{id}/delete", name="delete_post", methods={"DELETE"})
     */
    public function deleteAction(Request $request, BlogPost $blogPost): Response
    {
        // $blogPost = $this->blogPostRepository->findOneBy(['id' => $blogPost]);
        // $author = $this->userRepository->findOneBy(['account' => $this->getUser()->getAccount()]);
        // if (!$blogPost || $author !== $blogPost->getAuthor()) {
        //     $this->addFlash('error', 'You must be the author to delete this post');

        //     return $this->redirectToRoute('index');
        // }

        if ($this->isCsrfTokenValid('delete_post'.$blogPost->getId(), $request->request->get('_token'))) {
            $this->entityManager->remove($blogPost);
            $this->entityManager->flush();  

            $this->addFlash('success', 'Post deleted!');
        }

        // $this->entityManager->remove($blogPost);
        // $this->entityManager->flush();
    
        // $this->addFlash('success', 'Post deleted!');

        return $this->redirectToRoute('index');
    }
}
