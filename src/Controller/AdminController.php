<?php

namespace App\Controller;

use App\Repository\BlogPostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }
    
    /**
     * @Route("/admin/archives", name="admin_blogs")
     */
    public function showBlogPosts(BlogPostRepository $blogPostRepository): Response
    {
        return $this->render('admin/posts.html.twig', [
            'posts' => $blogPostRepository->findAllOrderByLatest()
        ]);
    }
}
