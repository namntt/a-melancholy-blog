<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\BlogPost;
use App\Repository\BlogPostRepository;

/**
 * @Route("/api")
 */
class APIController extends AbstractController
{
    private $entityManager;
    private $blogPostRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->blogPostRepository = $entityManager->getRepository('App:BlogPost');

    }
     /**
     * @Route("/posts", name="api_index")
     */
    public function postsAction()
    {   
        $blogPosts = $this->blogPostRepository->findBy(array(), array('createdAt' => 'DESC'), 5);
        $serializedPosts = [];
        foreach($blogPosts as $blogPost){
            array_push($serializedPosts, $this->serializeBlogPost($blogPost));
        }

        return new JsonResponse(['post' => $serializedPosts, 'items' => count($serializedPosts)]);
    }

    /**
     * 
     * @Route("/post/{slug}", name="api_view_post", methods={"GET"})
     */
    public function viewAction(string $slug): Response
    {
        $blogPost = $this->blogPostRepository->findOneBy(['slug' => $slug]);
        
        return new JsonResponse(['post' => $this->serializeBlogPost($blogPost)]);
    }

    private function serializeBlogPost(BlogPost $blogPost){
        return array(
            'title' => $blogPost->getTitle(),
            'slug' => $blogPost->getSlug(),
            'body' => $blogPost->getBody(),
            'createdAt' => $blogPost->getCreatedAt(), 
            'updatedAt' => $blogPost->getUpdatedAt()
        );
    }
}
