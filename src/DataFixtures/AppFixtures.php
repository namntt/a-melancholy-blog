<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\BlogPost;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $rolesUser[] = 'ROLE_USER';
        $rolesAdmin[] = 'ROLE_ADMIN';
        $admin = new User;
        $author = new User();
        $author2 = new User();
        $blogPost = new BlogPost();
        $blogPost2 = new BlogPost();

        $author->setName("Guillaume Musso")->setAccount("guillaume.musso")->setTitle("Novelist")->setEmail("guillaume.musso@gumu.com")
                ->setPassword($this->passwordEncoder->encodePassword($author,"guillaume"))->setRoles($rolesUser);
        $author2->setName("Marc Levy")->setAccount("marc.levy")->setTitle("Novelist")->setEmail("marc.levy@male.com")
                ->setPassword($this->passwordEncoder->encodePassword($author2,"marc"))->setRoles($rolesUser);
        $admin->setName("Admin")->setAccount("admin")->setTitle("Administrator")->setEmail("admin@amelancholyblog.com")
        ->setPassword($this->passwordEncoder->encodePassword($admin,"admin"))->setRoles($rolesAdmin);

        $blogPost->setTitle("Afterwords")
            ->setDescription("Life is for living, Life is for Love")
            ->setBody("At age 8, Nathan drowned when he jumped into a lake to save a girl’s life. He went into cardiac arrest, saw a light at the end of a tunnel, and was pronounced dead. Then, against all odds, he came back to life.
            Twenty years later, Nathan has become a successful lawyer in New York. Scarred by his divorce, he buries himself in his work, when a mysterious doctor suddenly appears. The time has come for him to discover, why he came back.")
            ->setAuthor($author);

        $blogPost2->setTitle("Une autre idée du bonheur")
            ->setDescription("Dans ce roman, Marc Levy réaffirme notre besoin inconditionnel de liberté et nous fait aussi découvrir un pan méconnu de l'histoire américaine.")
            ->setBody("Quand une vie ordinaire devient extraordinaire Philadelphie. Au premier jour du printemps 2010, Agatha sort de prison, mais pas par la grande porte. Après trente ans derrière les barreaux, il ne lui restait que quelques années à faire. 
                Alors pourquoi cette évasion ? Dans une station-service proche du campus, elle s’invite à bord de la voiture de Milly et l’entraîne dans sa cavale sans rien lui révéler de sa situation. Dotée d’un irrésistible appétit de vivre, Agatha fait voler en éclats la routine confortable de Milly. 
                Vingt ans les séparent, mais au fil du voyage les deux femmes partagent ces rêves qu’il n’est jamais trop tard pour réaliser et évoquent ces amours qui ne s’éteignent pas. Cinq jours en voiture à travers les États-Unis? À chaque étape, une rencontre avec un personnage surgi du passé les rapprochera du secret d’Agatha. 
                Jusqu’où devons-nous aller dans notre quête insatiable du bonheur? À quoi ne faut-il jamais renoncer ?")
            ->setAuthor($author2);

        $manager->persist($author);
        $manager->persist($author2);
        $manager->persist($admin);
        $manager->persist($blogPost);
        $manager->persist($blogPost2);

        $manager->flush();
    }
}
