var Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .cleanupOutputBeforeBuild()
    .addEntry('app', './assets/js/app.js')
    .enableSassLoader()
    .autoProvidejQuery()
    .enableSingleRuntimeChunk() 
    .enableSourceMaps(!Encore.isProduction());

module.exports = Encore.getWebpackConfig();