## Auteur
NGUYEN Tran Tuan Nam - M2 AIGLE

## Propose
- A melancholy blog a été réalisé dans le cadre du projet d'UE HMIN302. L'idée de ce blog est de collaboration, les utilisateurs contribuent des blog posts sur le site web.
- Lien du blog : https://nttn-melancholy-blog.herokuapp.com

## Fonctionnements
- CRU un blog post (La fonction 'Delete' a été réalisé dans le côté backend, mais elle ne marche pas avec le côté frontend)
- Il est obligé d'être l'auteur du post pour modifier un post.
- Registration d'un compte (role=user) puis sa connexion et sa déconnexion.
- Un API pour récoupérer les 5 derniers blog posts.

## Base de donnée utilisée
MySQL (local) et ClearDB (heroku).
